<?php
/**
 * @file
 * Contains \Drupal\mns\Form\Mns_form.
 */
namespace Drupal\mns\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;

class Mns_form extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mns_mns_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    // ------------------------------------Fetch all content types--------------------------------------------------------.
    $content_type['content_type'] = \Drupal::request()->query->get('content_type');
    $unpublishdatetime['unpublish'] = \Drupal::request()->query->get('unpublish');
    $publishdatetime['publish'] = \Drupal::request()->query->get('publish');
    $table = \Drupal::state()->get('table');
    $selected_rows = array();
    foreach ($table as $key => $value) {
      if ($value) {
        // The row with index $key is selected.
        $selected_rows[] = $key;
      }
  
    }
    $status = \Drupal::request()->query->get('status');
    $content_type_value = \Drupal::request()->query->get('node_type');
    $content_types = \Drupal::service('entity_type.manager')->getStorage('node_type')->loadMultiple();
    $content_type_options = ['-select-'];
    
    foreach ($content_types as $content_type) {
      $content_type_options[$content_type->id()] = $content_type->label();
    }
    $form['Node'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multi-Node Scheduler'),
      '#collapsible' => FALSE,
    ];
    $form['Node']['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content Type:'),
      '#required' => TRUE,
      '#options' => $content_type_options,
      '#default_value' => !empty($content_type)? $content_type : '',
      '#default_value' => ' ',
      '#suffix' => '<div class="error" id="mns_name"></div>'
    );
    // ---------------------------------------------status--------------------------------------------------------------
    $form['Node']['details']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Status'),
      '#description' => $this->t("Filter by node status"),
      '#empty_option' => '-select-',
      '#options' => [$this->t('Unpublish'), $this->t('Publish')],
      '#default_value' => $status,
    ];
    //-------------------------------------------unpublish/publish datetime--------------------------------
    $form['schedule_options'] = array(
      '#type' => 'details',
      '#title' => t('schedule options'),
      '#options' => ['select'],
      '#default_value' => ' ',
    );
    $form['datetime_wrapper'] = array(
      '#prefix' => '<div id="datetime-wrapper">',
      '#suffix' => '</div>',
    );
    // Date field 'unpublish' which will be visible only when radio 'unpublish' is selected.
    $form['schedule_options']['unpublish'] = [
      '#type' => 'datetime',
      '#title' => t('Date for unpublish'),
      '#date_format' => 'Y-m-d H:i:s',
      '#default_value' =>!empty($unpublishdatetime) ? $unpublishdatetime : '',
    ];
    // Date field 'publish' which will be visible only when radio 'publish' is selected.
    $form['schedule_options']['publish'] = [
      '#type' => 'datetime',
      '#title' => t('Date for publish'),
      '#date_format' => 'Y-m-d H:i:s',
      '#default_value' =>!empty($publishdatetime) ? $publishdatetime : '',
    ];
    // ----------------------------------submit----------------------------------------------------------------
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),

    ];
    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $this->header(),
      '#options' => $this->mns() ?: [],
      '#empty' => $this->t( 'No node found.' ),
    ];
    $form['pager'] = [
      '#type' => 'pager'
    ];
    // Prevent caching.
    $form['#cache']['max-age'] = 0;
    $form['#attached']['library'][] = 'mns/global_style';
    return $form;
  }

  // ------------------------------------------submitForm function----------------------------------------------------------
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_type = $form_state->getValue('content_type');
    // Get the datetime value from the submitted form state.
    $selected_rows = $form_state->getValue('table');
    \Drupal::state()->set('table' , $selected_rows);

    $unpublish = $form_state->getValue('unpublish');
    // Check if it's a valid DrupalDateTime object.
    if ($unpublish instanceof \Drupal\Core\Datetime\DrupalDateTime) {
      // Access date and time properties.
      $u_date = $unpublish->format('Y-m-d H:i:s.u e (P)');
      \Drupal::state()->set('date' , $u_date);
      // Now you can use $date and $time as needed.
    }

    $publish = $form_state->getValue('publish');
    if ($publish instanceof \Drupal\Core\Datetime\DrupalDateTime) {
      // Access date and time properties.
      $p_date = $publish->format('Y-m-d H:i:s.u e (P)');
      \Drupal::state()->set('p_date' , $p_date);
      // Now you can use $date and $time as needed.
    }
    
    \Drupal::state()->set('node_type' , $content_type);
    $unpublish_dates = [];
    foreach ($selected_rows as $nodeId => $value) {
      if ($value) {
        $unpublish_dates[$nodeId] = $unpublish;
      }
    }

      \Drupal::state()->set('unpublish_date' , $unpublish);
      \Drupal::state()->set('publish_date', $publish);

    $status = $form_state->getValue('status');

    $url = Url::fromRoute('mns.admin_form')->setRouteParameters(['content_type' => $content_type, 'unpublish' => $unpublish, 'publish' => $publish, 'status' => $status]);
    
    $form_state->setRedirectUrl($url);

  }
  // --------------------------------------------header function--------------------------------------------------------------
  /**
   * {@inheritdoc}
   */
  protected function header() {
  return [
      'title' => $this->t('Node Title'),
      'content_type' => $this->t('Content Type'),
      'status' => $this->t('Published/unpublish'),
      'unpublishdatetime' => $this->t('Unpublished Datetime'),
      'publishdatetime' => $this->t('published Datetime')
    ];
  }

  // -----------------------------------------mns function-------------------------------------------------------------------------------
  /**
   * {@inheritdoc}
   */
  function mns() {

    $content_type = \Drupal::request()->query->get('content_type');
    $unpublishdatetime = \Drupal::request()->query->get('unpublish');
    $publishdatetime = \Drupal::request()->query->get('publish');
    $table = \Drupal::state()->get('table');
    $unpublish_dates = [];
    $selected_rows = array();
    
    foreach ($table as $nodeId => $value) {
      if ($value) {
        // The row with index $key is selected.
        $selected_rows[] = $nodeId;
      }

    }

    $i = 0 ;
    $publish_dates = [];
    foreach ($selected_rows as $nodeId => $value) {
      if ($value) {
        $publish_dates[$value] = $publishdatetime;
      }
      $i++;
    }
      
    $status = \Drupal::request()->query->get('status');
      $query = \Drupal::database()->select('node_field_data', 'n');
      $query->fields('n', ['nid', 'type', 'status']);
      if(isset($content_type) && !empty($content_type))
      {
        $query->condition('n.type', $content_type);       
      }
      if(isset($status) && !empty($status) && !empty($content_type)){
        $query->condition('n.status',$status);
      }
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(6);
      $nids = $pager->execute()->fetchCol();                          
      $nid = [];
      foreach($nids as $ids) {
        $nid[] = $ids;
      }
      $nodes = Node::loadMultiple($nid);
     
    $rows = [];
    $i = 0 ;
      foreach ($selected_rows as $nodeId => $value) {
        
        if ($value) {
          $unpublish_dates[$value] = $unpublishdatetime;
        }
        $i++;
      }


    $i = 0;
    foreach ($nodes as $node)
    {

      $rows[] = [
        'title' => $node->getTitle(),
        'content_type' => $node->getType(),
        'status' => $node->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
        'unpublishdatetime' =>!empty($unpublish_dates)  ?  $unpublish_dates[$i] :'',
        'publishdatetime' =>!empty($publish_dates) ?  $publish_dates[$i] : '',
      ];
      $i++;
    }
    return $rows;
  }
}
