# Multi Node Scheduler Module

- This module is basically designed to Publish and unpublish multiple contents on specified dates and times.

## Table of contents

- Introduction
- Description
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- The Scheduler module is designed to schedule the of multiple 
  content of a particular content type within a Drupal environment.
  This module provides Users the facility to select the multiple rows
  and can set specific dates and time for these nodes to be published or 
  unpublished.

## Description

- Once you Install this module you can find the menu item
  named as MNS in the main Drupal admin menu bar. You can either 
  directly go to this MNS tab to configure the module settings or 
  navigate to:
  /admin/config/content/mns
- Now, User will be able to configure the module 
  settings as per their requirement.
- This module provides Users the facility
  to select the content type and its status to load all the 
  nodes of a particular content type with the status as selected.
- Once the nodes for the selected content type and status 
  have been loaded you can further select te multiple rows and can
  specify the dates and time for these nodes to be set as published or 
  unpublished.
- In this module the cron will run and the status will be updated.
  To run the cron you need to navigate to :
  Configuration > Settings > Cron. 
- There you can find the job created by the name of MNS job.
 
## Requirements

- This module requires ultimate cron module outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Once the module has been installed, navigate to
  /admin/config/content/mns.
- To run the cron you need to navigate to :
  (Configuration > Settings > Cron)

## Maintainers
- Sonam Sharma - [sonam_sharma](https://www.drupal.org/u/sonam_sharma)
- Prachi Jain - [prachi6824](https://www.drupal.org/u/prachi6824)
- Vijay Sharma - [vijaysharma_89](https://www.drupal.org/u/vijaysharma_89)
- Priyansh Chourasiya - [priyansh-chourasiya](https://www.drupal.org/u/priyansh-chourasiya)