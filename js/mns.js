(function ($, Drupal) {
  Drupal.behaviors.customForms = {
    attach: function (context, settings) {
      $('#edit-radio-options', context).once('mns').change(function () {
        var selectedValue = $(this).val();
        var $datetimeWrapper = $('#datetime-wrapper');

        if (selectedValue === 'unpublish') {
          $datetimeWrapper.show();
        } else {
          $datetimeWrapper.hide();
        }
      });
    }
  };
})(jQuery, Drupal);
